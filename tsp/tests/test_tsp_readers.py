import unittest
import pkg_resources
import warnings

import tsp
import tsp.readers as rdr
from tsp.core import IndexedTSP, TSP

pkg = "tsp"


class TestGeotopReader(unittest.TestCase):
    
    def setUp(self) -> None:
        self.file = pkg_resources.resource_filename(pkg, "data/example_geotop.csv")
        self.ws = pkg_resources.resource_filename(pkg, "data/test_geotop_has_space.txt")

    def test_reads_ok(self):
        self.assertTrue(isinstance(rdr.read_geotop(self.file), tsp.TSP))

    def test_has_whitespace(self):
        t = rdr.read_geotop(self.ws)
        self.assertEqual((3,1), t.values.shape)


class TestGtnpReader(unittest.TestCase):
    
    def setUp(self) -> None:
        self.file = pkg_resources.resource_filename(pkg, "data/example_gtnp.csv")
        self.t2023m = pkg_resources.resource_filename(pkg, "data/2023-01-06_755-test.metadata.txt")
        self.t2023 = pkg_resources.resource_filename(pkg, "data/2023-01-06_755-test-Dataset_2031-Constant_Over_Interval-Hourly-Ground_Temperature-Thermistor_Automated.timeserie.csv")

    def test_reads_ok(self):
        self.assertTrue(isinstance(rdr.read_gtnp(self.file), tsp.TSP))
        self.assertTrue(isinstance(rdr.read_gtnp(self.t2023), tsp.TSP))

    def test_metadata(self):
        t = rdr.read_gtnp(filename=self.t2023, metadata_filepath=self.t2023m)
        self.assertIsNotNone(t.longitude)
        self.assertIsNotNone(t.latitude)
        self.assertIsNotNone(t.utc_offset)
        self.assertEqual(-9 * 3600, t.utc_offset.utcoffset(None).total_seconds())
        
    def test_autodetect_metadata(self):
        t = rdr.read_gtnp(filename=self.t2023)
        self.assertIsNotNone(t.longitude)
        self.assertIsNotNone(t.latitude)
        self.assertLess(0, len(t.metadata))


class TestHoboReader(unittest.TestCase):
    
    def setUp(self) -> None:
        warnings.simplefilter('ignore', category=UserWarning)
        self.file = pkg_resources.resource_filename(pkg, "dataloggers/test_files/hobo_1_AB_classic.csv")
        self.hobo = rdr.read_hoboware(self.file)
    
    def test_reads_ok(self):
        self.assertTrue(isinstance(self.hobo, IndexedTSP))
        self.assertTrue(isinstance(self.hobo, tsp.TSP))

    def test_timezone(self):
        self.assertIsNotNone(self.hobo.utc_offset)
        self.assertEqual(-7 * 3600 , self.hobo.utc_offset.utcoffset(None).total_seconds())
        self.assertEqual(14, self.hobo.times[0].hour)


class TestGeoprecisionReader(unittest.TestCase):
    
    def setUp(self) -> None:
        self.fg2_399 = pkg_resources.resource_filename(pkg, "dataloggers/test_files/FG2_399.csv")
        self.gp_26 = pkg_resources.resource_filename(pkg, "dataloggers/test_files/GP5W_260.csv")
        self.gp_27 = pkg_resources.resource_filename(pkg, "dataloggers/test_files/GP5W_270.csv")

    def test_reads_ok(self):
        self.assertTrue(isinstance(rdr.read_geoprecision(self.fg2_399), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_geoprecision(self.gp_26), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_geoprecision(self.gp_27), IndexedTSP))


class TestRbrReader(unittest.TestCase):

    def setUp(self) -> None:
        """
        test files still needed:
        XL-800 logger file with date stamps on each line
        XR-420 logger file with line numbers

        """

        # round 1 tests
        self.rbr_xl_1_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_xl_001.DAT")
        self.rbr_xl_2_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_xl_002.DAT")
        self.rbr_xl_3_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_xl_003.DAT")
        self.rbr_xl_3_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_xl_003.HEX")

        self.rbr_1_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_001.dat")
        self.rbr_1_nc_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_001_no_comment.dat")
        self.rbr_1_nc_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_001_no_comment.hex")
        self.rbr_1_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_001.hex")
        self.rbr_2_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_002.dat")
        self.rbr_2_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_002.hex")
        self.rbr_3_xls = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_003.xls")
        self.rbr_3_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/rbr_003.hex")

        # round 2 tests
        self.rbr_xl_004448_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/004448.DAT")
        self.rbr_xl_004531_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/004531.DAT")
        self.rbr_xl_004531_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/004531.HEX")
        self.rbr_xl_004534_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/004534.HEX")
        self.rbr_010252_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/010252.dat")
        self.rbr_010252_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/010252.hex")
        self.rbr_010274_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/010274.hex")
        self.rbr_010278_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/010278.hex")
        self.rbr_012064_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/012064.dat")
        self.rbr_012064_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/012064.hex")
        self.rbr_012081_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/012081.hex")
        self.rbr_062834_rsk = pkg_resources.resource_filename(pkg, "dataloggers/test_files/062834_20220904_2351.rsk")
        self.rbr_062834_xlsx = pkg_resources.resource_filename(pkg, "dataloggers/test_files/062834_20220904_2351.xlsx")
        self.rbr_xl_07B1592_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/07B1592.HEX")
        self.rbr_xl_07B1592_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/07B1592.DAT")
        self.rbr_xl_07B4450_hex = pkg_resources.resource_filename(pkg, "dataloggers/test_files/07B4450.HEX")
        self.rbr_xl_07B4450_dat = pkg_resources.resource_filename(pkg, "dataloggers/test_files/07B4450.DAT")

    def test_reads_ok(self):
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_1_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_2_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_3_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_3_hex), IndexedTSP))

        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_1_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_1_nc_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_2_dat), IndexedTSP))

        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_1_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_2_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_3_hex), IndexedTSP))
        # self.assertTrue(isinstance(rdr.read_rbr(self.rbr_1_nc_hex), IndexedTSP))

        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_3_xls), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_004448_dat), IndexedTSP))
        # rdr.read_rbr(self.rbr_xl_004531_dat)  # corrupt data
        # rdr.read_rbr(self.rbr_xl_004531_hex)  # corrupt data
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_004534_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_010252_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_010252_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_010274_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_010278_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_012064_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_012064_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_012081_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_062834_rsk), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_062834_xlsx), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_07B1592_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_07B1592_dat), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_07B4450_hex), IndexedTSP))
        self.assertTrue(isinstance(rdr.read_rbr(self.rbr_xl_07B4450_dat), IndexedTSP))
        return


class TestNTGSReader(unittest.TestCase):

    def setUp(self) -> None:
        self.ntgs = pkg_resources.resource_filename(pkg, "data/NTGS_example_csv.csv")
        self.ntgs_slash_dates = pkg_resources.resource_filename(pkg, "data/NTGS_example_slash_dates.csv")

    def test_reads_ok(self):
        self.assertTrue(isinstance(rdr.read_ntgs(self.ntgs), tsp.TSP))
        self.assertTrue(isinstance(rdr.read_ntgs(self.ntgs_slash_dates), tsp.TSP))


class TestPermosReader(unittest.TestCase):

    def setUp(self) -> None:
        self.test_file = pkg_resources.resource_filename(pkg, "data/example_permos.csv")

    def test_reads_ok(self):
        self.assertTrue(isinstance(rdr.read_permos(self.test_file), tsp.TSP))

    def test_read_correct(self):
        t = rdr.read_permos(self.test_file)
        self.assertEqual(11, len(t.depths))
        self.assertEqual(0.5, t.depths[1])


if __name__ == '__main__':
    unittest.main()
