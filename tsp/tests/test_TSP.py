import unittest
import datetime
import numpy as np
import pandas as pd

from tsp.core import TSP

pkg = "tsp"
np.random.seed(0)

class TestTSP(unittest.TestCase):
    
    def setUp(self) -> None:
        self.depths = np.array([1,3,5,7])
        self.times = pd.date_range(start="2000-01-01T00:00:00", periods=200, freq='D')
        self.values = np.random.normal(size=(200,4))
        self.t = TSP(times = self.times, depths=self.depths, values=self.values)

    def test_inputs_same_length(self):
        self.assertEqual(len(self.t.times), len(self.times))
        self.assertEqual(len(self.t.depths), len(self.depths))
        self.assertEqual(len(self.t.values), len(self.values))

    def test_inputs_unchanged(self):
        self.assertEqual(datetime.timedelta(0), np.sum(np.subtract(self.t.times, self.times)))
        self.assertEqual(0, np.sum(self.t.depths - self.depths))
        self.assertEqual(0, np.sum(self.t.values - self.values))

    def test_monthly(self):
        self.assertGreater(self.t.long.shape[0], self.t.monthly().long.shape[0])

    def test_daily(self):
        self.assertGreaterEqual(self.t.long.shape[0], self.t.daily().long.shape[0])
    
class TestCache(unittest.TestCase):
    
    def setUp(self) -> None:
        self.depths = np.array([1,3,5,7])
        self.times = pd.date_range(start="2000-01-01T00:00:00", periods=200, freq='D').to_pydatetime()
        self.values = np.random.normal(size=(200,4))
        
    def test_change_depths_wide(self):
        t = TSP(times = self.times, depths=self.depths, values=self.values)
        t.wide  # set cache
        t.depths = np.array([10,15,20,25])
        self.assertEqual(set(np.array([10,15,20,25])), set(t.wide.columns[1:]))

    def test_change_depths_long(self):
        t = TSP(times = self.times, depths=self.depths, values=self.values)
        t.long  # set cache
        t.depths = np.array([10,15,20,25])
        self.assertEqual(set(np.array([10,15,20,25])), set(t.long['depth'].unique()))

class TestCreation(unittest.TestCase):

    def setUp(self) -> None:
        base = datetime.datetime.today()
        date_list = [base - datetime.timedelta(days=x) for x in range(5)]
        self.times = np.array(date_list + date_list)
        self.depths = np.array([1,1,1,1,1,2,2,2,2,2])
        self.values = np.array([1,2,3,4,5,-5,-4,-3,-2,-1])
    
    def testFromTidy(self):
        t = TSP.from_tidy_format(times=self.times, depths=self.depths, values=self.values)
        self.assertEqual(2, len(t.depths))
        self.assertEqual(5, len(t.times))
        self.assertEqual((5,2), t.values.shape)
        self.assertEqual(3, np.sum(t.depths))
        self.assertEqual(2, int(t.depths[1]))

    def test_synthetic(self):
        t = TSP.synthetic(depths = np.array([0, 0.5, 1, 1.5, 3, 5]))
        self.assertEqual(6, len(t.depths))
        self.assertTrue(isinstance(t, TSP))


class TestTimeZones(unittest.TestCase):

    def setUp(self) -> None:
        pass

    def test_set_time_zone(self):
        t = TSP.synthetic(depths = np.array([0, 0.5, 1, 1.5, 3, 5]))
        self.assertIsNone(t.utc_offset)
        self.assertIsNone(t.times.tz)

    def test_set_utc_offset_string(self):
        t = TSP.synthetic(depths = np.array([0, 0.5, 1, 1.5, 3, 5]))
        t.set_utc_offset("-01:00")
        self.assertEqual(-1 * 60 * 60, t.utc_offset.utcoffset(None).total_seconds())
        self.assertEqual(-1 * 60 * 60, t.times.tz.utcoffset(None).total_seconds())
    
    def test_set_utc_offset_int(self):
        t = TSP.synthetic(depths = np.array([0, 0.5, 1, 1.5, 3, 5]))
        t.set_utc_offset(-1 * 60 * 60)
        self.assertEqual(-1 * 60 * 60, t.utc_offset.utcoffset(None).total_seconds())
        self.assertEqual(-1 * 60 * 60, t.times.tz.utcoffset(None).total_seconds())

    def test_already_set(self):
        t = TSP.synthetic(depths = np.array([0, 0.5, 1, 1.5, 3, 5]))
        t.set_utc_offset(-1 * 60 * 60)
        with self.assertRaises(ValueError):
            t.set_utc_offset(-1 * 60 * 60)

    def test_output_times(self):
        t = TSP.synthetic(depths = np.array([0, 0.5, 1, 1.5, 3, 5]))
        t.set_utc_offset(-1 * 60 * 60)
        t.set_output_utc_offset("UTC")
        self.assertEqual(t.times[0].hour, t._times[0].hour + 1)

    def test_output_same_numeric_value(self):
        t = TSP.synthetic(depths = np.array([0, 0.5, 1, 1.5, 3, 5]))
        t.set_utc_offset(-1 * 60 * 60)
        t.set_output_utc_offset("UTC")
        self.assertEqual(t.times[0].toordinal(), t._times[0].toordinal())
        self.assertEqual(t.times[-1].toordinal(), t._times[-1].toordinal())

class testInputTimesWithTimezone(unittest.TestCase):

    def setUp(self) -> None:
        self.d = [1,2]
        self.v = np.array([[4,5],[6,7]])
        self.fromstr = pd.to_datetime(["2000-01-01T00:00:00-0400","2000-01-02T00:00:00-0400"], format="%Y-%m-%dT%H:%M:%S%z")
        self.from_datetimearray = pd.to_datetime(np.array([datetime.datetime(year=2000, month=1, day=1,
                                                                             hour=0, minute=0, second=0),
                                                           datetime.datetime(year=2000, month=1, day=2,
                                                                             hour=0, minute=0, second=0)])
                                                 ).tz_localize(datetime.timezone(datetime.timedelta(hours=-4)))

    
    def test_from_str(self):
        t = TSP(self.fromstr, self.d, self.v)
        self.assertEqual(t.times[0].hour, 0)

    def test_from_datetimearray(self):
        t = TSP(self.from_datetimearray, self.d, self.v)
        self.assertEqual(t.times[0].hour, 0)


if __name__ == '__main__':
    unittest.main()


