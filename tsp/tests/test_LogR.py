""" Unit tests for readers"""
import pkg_resources
import unittest
from tsp.dataloggers import logr as lr

pkg = "tsp"


class TestLineDetection(unittest.TestCase):

    def test_detect_date(self):
        self.assertTrue(lr.is_data_row(",2020/02/26 12:50:00,22.329999923706055,92.06414031982422,17.904296875,3.6451704502105713,0,24,1012,20.591,20.362,20.434,20.437,20.557,20.240,20.298,20.352,20.394,20.328,20.469,20.368,20.316,20.545,20.792,20.363"))
        self.assertFalse(lr.is_data_row("SensorId,timestamp,temperature,pressure,humidity,battery,accelX,accelY,accelZ,CH1,CH2"))
        self.assertFalse(lr.is_data_row("Label,Na,Na,Na,Na,Na,Na,Na,Na,Air,0m,-0.5m,"))
        self.assertFalse(lr.is_data_row("Group,Na,Int,Int,Int,Int,Int,Int,Int,None,None"))
        self.assertFalse(lr.is_data_row("x,Na,Na,Na,Na,Na,Na,Na,Na,0,0,0,0,"))

    def test_detect_header(self):
        self.assertFalse(lr.is_columns_row(",2020/02/26 12:50:00,22.329999923706055,92.06414031982422,17.904296875,3.6451704502105713,0,24,1012,20.591,20.362,20.434,20.437,20.557,20.240,20.298,20.352,20.394,20.328,20.469,20.368,20.316,20.545,20.792,20.363"))
        self.assertTrue(lr.is_columns_row("SensorId,timestamp,temperature,pressure,humidity,battery,accelX,accelY,accelZ,CH1,CH2"))
        self.assertFalse(lr.is_columns_row("Label,Na,Na,Na,Na,Na,Na,Na,Na,Air,0m,-0.5m,"))
        self.assertFalse(lr.is_columns_row("Group,Na,Int,Int,Int,Int,Int,Int,Int,None,None"))
        self.assertFalse(lr.is_columns_row("x,Na,Na,Na,Na,Na,Na,Na,Na,0,0,0,0,"))

    def test_detect_label(self):
        self.assertFalse(lr.is_label_row(",2020/02/26 12:50:00,22.329999923706055,92.06414031982422,17.904296875,3.6451704502105713,0,24,1012,20.591,20.362,20.434,20.437,20.557,20.240,20.298,20.352,20.394,20.328,20.469,20.368,20.316,20.545,20.792,20.363"))
        self.assertFalse(lr.is_label_row("SensorId,timestamp,temperature,pressure,humidity,battery,accelX,accelY,accelZ,CH1,CH2"))
        self.assertTrue(lr.is_label_row("Label,Na,Na,Na,Na,Na,Na,Na,Na,Air,0m,-0.5m,"))
        self.assertFalse(lr.is_label_row("Group,Na,Int,Int,Int,Int,Int,Int,Int,None,None"))
        self.assertFalse(lr.is_label_row("x,Na,Na,Na,Na,Na,Na,Na,Na,0,0,0,0,"))


class TestGuessing(unittest.TestCase):
    
    def test_guess_depths(self):
        self.assertEqual([1, 3, 4], lr.guess_depths(["1", "3", "4"]))
        self.assertEqual([1, 3, 4], lr.guess_depths(["1m", "3m", "4m"]))
        self.assertEqual([-1, -3, -4], lr.guess_depths(["-1", "-3", "-4 metres"]))
        self.assertEqual([None, 1, 3, 4, None], lr.guess_depths(["Na", "1", "3", "4", "Na"]))
        self.assertEqual([-1.6, 3, 4], lr.guess_depths(["-1.6", "3", "4"]))

if __name__ == "__main__":
    unittest.main()