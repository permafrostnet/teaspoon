import unittest
import numpy as np
import pandas as pd
import warnings

from tsp import resolve_duplicate_times, TSP


class TestDuplicates(unittest.TestCase):
    
    def setUp(self) -> None:
        warnings.simplefilter('ignore', category=UserWarning)
        self.depths = np.array([1, 2])
        times = pd.date_range(start="2000-01-01T00:00:00", periods=3, freq='D').to_series()
        self.times = pd.concat([times[0:1],times[0:1],times,times[2:3]]).index
        
        self.values = np.array([[1, 2], [4, np.nan], [16, 32], [0, 0], [-1, -2], [-4, -8]])

        self.t = TSP(times=self.times, 
                     depths=self.depths, 
                     values=self.values,
                     metadata={'test': 'test'}, 
                     latitude=0, longitude=0)


    def test_keep_last(self):
        t = resolve_duplicate_times(self.t, 'last')
        self.assertEqual(3, len(t.times))
        self.assertEqual(32, t.wide.loc[self.times[0], 2])
        

    def test_keep_first(self):
        t = resolve_duplicate_times(self.t, 'first')
        self.assertEqual(3, len(t.times))
        self.assertEqual(2, t.wide.loc[self.times[0], 2])


    def test_keep_none(self):
        t = resolve_duplicate_times(self.t, 'strip')
        self.assertEqual(1, len(t.times))
        self.assertEqual(0, t.wide.loc[self.times[3], 2])


    def test_average(self):
        t = resolve_duplicate_times(self.t, 'average')
        self.assertEqual(3, len(t.times))
        self.assertEqual(17, t.wide.loc[t.times[0], 2])
        self.assertEqual(0, t.wide.loc[t.times[1], 2])
        self.assertEqual(-2.5, t.wide.loc[t.times[2], 1])