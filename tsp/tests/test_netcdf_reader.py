import numpy as np
from netCDF4 import Dataset
from os import remove

import unittest

import tsp
import tsp.readers as rdr


class TestNetCDFReader(unittest.TestCase):

    def setUp(self) -> None:
        nc = Dataset('inmemory.nc', mode='w', diskless=True, persist=True)
        nc.createDimension('time', 10)
        nc.createDimension('depth', 2)
        nc.featureType = 'timeSeriesProfile'

        time = nc.createVariable('time', 'f8', ('time',))
        time.units = 'seconds since 2018-01-01 00:00:00 -04:00'
        time.calendar = 'standard'
        time.axis = "T"
        time[:] = np.arange(10) * 3600
        
        depth = nc.createVariable('depth', 'f8', ('depth',))
        depth.axis = "Z"
        depth[:] = np.arange(2)

        temp = nc.createVariable('temp', 'f8', ('time', 'depth',))
        temp.units = "C"
        temp.standard_name = "temperature_in_ground"
        temp[:] = np.random.rand(10, 2)

        self.nc_buf = nc.close() # close returns memoryview

    def test_reads_netcdf(self):
        t = rdr.read_netcdf('inmemory.nc')
        self.assertTrue(isinstance(t, tsp.TSP))
        self.assertEqual(2, len(t.depths))
        self.assertEqual(10, len(t.times))

    def tearDown(self):
        remove('inmemory.nc')
