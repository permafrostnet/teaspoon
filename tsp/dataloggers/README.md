# Format
Reader objects save csv data in a pandas dataframe stored as the `DATA` attribute.  Column titles are left unchanged with the exception of the datetime column which is renamed to `TIME`. It is always the first column in the dataframe.

Where possible, any metadata that is found in the file is stored in a `META` attribute.

See docstring for AbstractReader for more information
