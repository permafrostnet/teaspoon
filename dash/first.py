# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

from dash import Dash, html, dcc, Input, Output, State
import plotly.express as px
import pandas as pd
import base64
import io

from teaspoon.plots.timeseries import simple_timeseries
from teaspoon.readers import read_gtnp, read_geotop

app = Dash(__name__)

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

opts = {'GTN-P': read_gtnp,
        'geotop model output': read_geotop}

df = pd.DataFrame({
    "Fruit": ["Apples", "Oranges", "Bananas", "Apples", "Oranges", "Bananas"],
    "Amount": [4, 1, 2, 2, 4, 5],
    "City": ["Ottawa", "Ottawa", "Ottawa", "Montreal", "Montreal", "Montreal"]
})

fig = px.bar(df, x="Fruit", y="Amount", color="City", barmode="group")

app.layout = html.Div(children=[
    html.H1(children='Permafrost'),
        
      dcc.Tabs([
        
        dcc.Tab(label="Tab one", children=[
            dcc.Dropdown(list(opts.keys()),
                        # options={'GTN-P': read_gtnp,
                        #          'geotop model output': read_geotop}, 
                         value=list(opts.keys())[1], id='upload-type', placeholder="Select upload type"),

        dcc.Upload(
            id='upload-data',
            children=html.Div([
                'Drag and Drop or ',
                html.A('Select Files')
            ]),
            style={
                'width': '100%',
                'height': '60px',
                'lineHeight': '60px',
                'borderWidth': '1px',
                'borderStyle': 'dashed',
                'borderRadius': '5px',
                'textAlign': 'center',
                'margin': '10px'
            },
            # Allow multiple files to be uploaded
            multiple=True
        ),

        html.Div(id='loaded-file'),

        dcc.Dropdown(['Trumpet curve', 'Time series'], value='Time series', id='graph-type-selector', placeholder="Select figure type"),

        html.Div(children='''
            Dash: A web application framework for your data.
        '''),


        dcc.Graph(
            id='main-graph',
            figure=fig
        )
        ]),

        dcc.Tab(label="Tab two", children=[
            html.H1("Data Selection"),
            html.P("data"),
            dcc.Checklist(id="selected-tsp",
                          options=['New York City', 'Montréal', 'San Francisco'],
                          value=['New York City', 'Montréal']
            )
            ])
        ])
])

# @app.callback(
#     Output(component_id="selected-tsp", component_property="options"),
#     Input(component_id="upload-data", component_property="")
# )
# def update_data_selection(input_value):
#     return 


@app.callback(
    Output(component_id='loaded-file', component_property='children'),
    Input(component_id="upload-data", component_property="filename"),
)
def update_output_div(input_value):
    return f'Output: {input_value}'


@app.callback(
    Output(component_id="main-graph", component_property="figure"),
    Input(component_id="upload-data", component_property="contents"),
    Input(component_id="upload-type", component_property="value")
)
def update_chart(contents, upload_type):
    try:
        reader = get_reader_from_upload_type(upload_type)
        content_type, content_string = contents[0].split(',')
        decoded = base64.b64decode(content_string)
        t = reader(io.StringIO(decoded.decode('utf-8')))
        return simple_timeseries(t)
    except Exception:
        df = pd.DataFrame({
    "Fruit": ["Apples", "Oranges", "Bananas", "Apples", "Oranges", "Bananas"],
    "Amount": [4, 1, 2, 2, 4, 5],
    "City": ["Ottawa", "Ottawa", "Ottawa", "Montreal", "Montreal", "Montreal"]
})

        fig = px.bar(df, x="Fruit", y="Amount", color="City", barmode="group")
        return fig

# @app.callback(Output('output-image-upload', 'children'),
#               Input('upload-image', 'contents'),
#               State('upload-image', 'filename'),
#               State('upload-image', 'last_modified'))
# def update_output(list_of_contents, list_of_names, list_of_dates):
#     if list_of_contents is not None:
#         children = [
#             parse_contents(c, n, d) for c, n, d in
#             zip(list_of_contents, list_of_names, list_of_dates)]
#         return children
def get_reader_from_upload_type(upload_type):
    return opts[upload_type]

if __name__ == '__main__':
    app.run_server(debug=True)