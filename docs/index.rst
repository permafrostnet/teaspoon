.. tsp documentation master file, created by
   sphinx-quickstart on Wed Jun 22 13:56:18 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for tsp
====================================


.. image:: https://joss.theoj.org/papers/10.21105/joss.04704/status.svg
   :target: https://doi.org/10.21105/joss.04704


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   source/about
   source/install
   source/examples
   source/contributions
   source/citing

.. toctree::
   :maxdepth: 2
   :caption: Functions and Classes:
   
   source/tsp
   source/readers
   source/plots
   source/dataloggers
   source/physics

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
