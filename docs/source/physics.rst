.. _physics:

Physics
==================

These functions provide physical equations that either can be accessed directly or are used to support the :py:class:`~tsp.core.TSP` class and subclasses.


.. automodule:: tsp.physics
   :members:
   :no-undoc-members:
   :show-inheritance: