About
=====

What is it?
-----------
``tsp`` ("teaspoon") is a python library designed to make working with permafrost time series data more straightforward, efficient, and reproduceable. It is described in `this JOSS paper <https://doi.org/10.21105/joss.04704>`_. Some of the features include:

* Read a variety of common published data formats, datalogger outputs, and model results into a common data structure
    * GEOtop model output
    * GTN-P database export csv
    * NTGS ground temperature report csv
    * Geoprecision datalogger export
    * HoboWare datalogger export
    * logR datalogger export
* Export data in a variety of common formats
    * netcdf
    * 'GTN-P'-style csv
    * 'NTGS'-style csv
* Perform common data transformations
    * Calculate daily, monthly, or yearly means, ignoring averaging periods with missing data
    * Switch between "long" and "wide" dataframes
* Visualize and explore your data with commonly used plots
    * Trumpet curves
    * Temperature-time graphs
    * Colour-contour profiles

.. figure:: ../_static/images/summary.png
    :width: 700
    :alt: A summary of the capabilities of the tsp library
    :align: center

    Figure 1: A summary of the main capabilities of the tsp library


Why "teaspoon" (tsp)?
---------------------

- The data represent the **T**\ hermal **S**\ tate of **P**\ ermafrost (or alternatively the **T**\ emperature du **S**\ ol en **P**\ rofondeur)
- The data are easily represented by the "**T**\ ime **S**\ eries of **P**\ rofiles" `CF discrete sampling geometry <https://cfconventions.org/cf-conventions/cf-conventions.html#time-series-profiles>`_
- It's short and memorable!


Where do I find it? 
-------------------
The code repository is hosted on `Gitlab <https://gitlab.com/permafrostnet/teaspoon>`_

How can I install it?
---------------------
See the :ref:`Installation` page of this documentation.