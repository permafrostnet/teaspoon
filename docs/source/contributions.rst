Contributing
=============

Contributions to ``tsp`` are welcome. Some possible contributions are suggested below.  Also make sure you read the notes in **Instructions for contributors**.

Suggestions for contributions
-----------------------------
- Better documentation: *Do you have a great example or application? Write a documentation page!*
- Support for more dataloggers: *Do you use a data logger that is not currently supported? Create a new reader for it*
- Examples of data logger text file outputs: *These are valuable because they help capture the variability of these files 'in the wild'. This, in turn makes it possible to write better tests and make sure they the readers work for everyone.* 
- New visualizations
- Add relevant parameters to existing visualizations
- Report any bugs, suggestions or unexpected behaviour to the `gitlab issue tracker <https://gitlab.com/permafrostnet/teaspoon/-/issues>`_


Instructions for contributors
-----------------------------
- Report any bugs, suggestions or unexpected behaviour to the `gitlab issue tracker <https://gitlab.com/permafrostnet/teaspoon/-/issues>`_
- Please follow the `python style guide <https://peps.python.org/pep-0008/>`_
- Follow the `Numpy convention <https://numpydoc.readthedocs.io/en/latest/format.html>`_ for docstrings. These are automatically converted into web documentation using `Sphinx <https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html>`_
- Make your code in a separate branch on Gitlab and `create a merge request <https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html>`_ when you think its ready to be added
- Where possible, add tests to make sure your code works as expected and continues to work as expected


