Datalogger Handlers
===================

.. currentmodule:: tsp.dataloggers


Geoprecision
------------
.. autofunction:: detect_geoprecision_type
   

.. autoclass:: GP5W
   :members: 
   :no-undoc-members:
   :show-inheritance:

.. autoclass:: FG2
   :members: 
   :no-undoc-members:
   :show-inheritance:


HoboWare
--------

.. autoclass:: HOBO
   :members: 
   :no-undoc-members:
   :show-inheritance:

.. autoclass:: HOBOProperties
   :members: 
   :no-undoc-members:
   :show-inheritance:

   
LogR
----

.. autoclass:: LogR
   :members: 
   :no-undoc-members:
   :show-inheritance:
