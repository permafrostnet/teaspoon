Examples
=========

This page shows some examples of how to use ``tsp`` for data exploration.

Loading data
^^^^^^^^^^^^
The easiest way to load data into a ``TSP`` is with one of the ``read_`` functions (described in :ref:`Readers`).

.. code-block:: python

    from tsp.readers import read_gtnp, read_geotop, read_csv, read_ntgs
    import pkg_resources

    ## Read common CSV exports
    data = read_geotop(pkg_resources.resource_filename('tsp', 'data/example_geotop.csv'))


Manipulating data
^^^^^^^^^^^^^^^^^
A ``TSP`` has several features that can make it easy to work with the data

Accessing data
---------------
Use the ``.long`` and ``.wide`` attributes to get the data in the "long" (tidy) or "wide" format.

.. code-block:: python

     data.long[1:9]


+----+----------------------------+---------+-------------------------+
|    | time                       |   depth |   temperature_in_ground |
+====+============================+=========+=========================+
|  1 | 2021-05-06 16:04:33.770647 |       1 |                    -0.5 |
+----+----------------------------+---------+-------------------------+
|  2 | 2020-05-06 16:04:33.770647 |       1 |                    -0.4 |
+----+----------------------------+---------+-------------------------+
|  3 | 2022-05-06 16:04:33.770647 |       5 |                    -3   |
+----+----------------------------+---------+-------------------------+
|  4 | 2021-05-06 16:04:33.770647 |       5 |                    -2   |
+----+----------------------------+---------+-------------------------+
|  5 | 2020-05-06 16:04:33.770647 |       5 |                    -1.8 |
+----+----------------------------+---------+-------------------------+
|  6 | 2022-05-06 16:04:33.770647 |      10 |                    -5   |
+----+----------------------------+---------+-------------------------+
|  7 | 2021-05-06 16:04:33.770647 |      10 |                    -4.9 |
+----+----------------------------+---------+-------------------------+
|  8 | 2020-05-06 16:04:33.770647 |      10 |                    -4.8 |
+----+----------------------------+---------+-------------------------+


.. code-block:: python

    data.wide[1:3]


+----------------------------+----------------------------+------+------+------+
|                            | time                       |    1 |    5 |   10 |
+============================+============================+======+======+======+
| 2021-05-06 16:04:33.770647 | 2021-05-06 16:04:33.770647 | -0.5 | -2   | -4.9 |
+----------------------------+----------------------------+------+------+------+
| 2020-05-06 16:04:33.770647 | 2020-05-06 16:04:33.770647 | -0.4 | -1.8 | -4.8 |
+----------------------------+----------------------------+------+------+------+


Alternatively, you can access the original data (``.depths``, ``.times``, and ``.values``) as arrays:

.. code-block:: python

    >>> data.depths
    array([ 0.1,  1. ,  5. , 10. ])
    
    >>> data.times
    array(['2016-05-31T06:00:00.000000000', '2016-05-31T12:00:00.000000000',
       '2016-05-31T18:00:00.000000000', ...,
       '2019-12-31T12:00:00.000000000', '2019-12-31T18:00:00.000000000',
       '2019-12-31T23:00:00.000000000'], dtype='datetime64[ns]')

    >>> data.values
    array([[-16.473,  -9.27 ,  -0.123,   1.289],
           [-13.206,  -9.289,  -0.123,   1.289],
           [ -6.795,  -9.308,  -0.123,   1.289],
           ...,
           [-18.027,  -9.295,  -0.11 ,   1.142],
           [-17.095,  -9.315,  -0.11 ,   1.142],
           [-16.501,  -9.33 ,  -0.11 ,   1.142]])


Averages
--------
You can produce monthly or daily averages of data using the ``.daily()`` and ``.monthly()`` methods. Keep in mind that these return ``TSP`` objects, not DataFrames! You need to use either ``.wide`` or ``.long`` to access the data.


.. code-block:: python

    >>> data.monthly().wide
    
+-----------+----------+----------+----------+-----------+---------+
|           |      time|       0.1|       1.0|       5.0 |     10.0|
+===========+==========+==========+==========+===========+=========+
|2016-06-01 |2016-06-01|  3.711758| -6.167192| -0.118442 | 1.287075|
+-----------+----------+----------+----------+-----------+---------+
|2016-07-01 |2016-07-01|  8.930806| -0.795556| -0.131282 | 1.282887|
+-----------+----------+----------+----------+-----------+---------+
|2016-08-01 |2016-08-01|  7.942976|  1.774726| -0.154524 | 1.278685|
+-----------+----------+----------+----------+-----------+---------+

Plotting data
^^^^^^^^^^^^^
Each ``TSP`` object can be visualized with a single command for easy data exploration. The most common data visualizations are provided. Each of the plotting methods begins with ``plot_`` and they use the functions defined in :ref:`plots`, which can also be accessed directly.


.. raw:: html
    :file: ../_static/examples_plotting.html

Handling Time Zones (UTC offset)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


.. raw:: html
    :file: ../_static/examples_timezones.html

