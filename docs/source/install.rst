.. _installation:

Installation
============
Note that all installation steps should be done from the bash shell or windows command prompt.

Using pip
---------
To install tsp, the easiest way is to use pip: ::

    pip install tsp


tsp requires additional dependencies to unlock all its capabilities. To install tsp along with all the optional dependencies, specify::

    pip install tsp[plotting,nc]


Here `nc` installs packages to handle netcdf files and `plotting` installs packages to help with plotting. You may include only one or the other depending on your needs.


From source
-----------
You can also install tsp from the source code. This will give you the latest version, but there may be some bugs. From the command-line prompt, use the following commands: ::

    git clone https://gitlab.com/permafrostnet/teaspoon
    cd teaspoon
    pip install .[plotting,nc] -vvv


Testing 
^^^^^^^
``tsp`` uses the ``unittest`` package for testing. To test a source install, use shell or the windows command prompt to enter the following commands from the root of the cloned repository directory::

    cd tsp/tests
    python -m unittest

If you find that the tests all run, but the code does not behave as expected, `file a bug report in a new issue <https://gitlab.com/permafrostnet/teaspoon/-/issues/new>`_ or add a new test and `contribute it with a merge request <https://subscription.packtpub.com/book/application-development/9781783986842/4/ch04lvl1sec34/creating-your-first-merge-request>`_
