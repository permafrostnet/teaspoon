.. _plots:

Plotting functions
==================

In addition to accessing plotting capabilities through the :py:class:`tsp.core.TSP` class and subclasses, the plotting functions can be accessed directly. These functions are datasource-agnostic, so they can be used independently of any other functions or classes in this package.

.. currentmodule:: tsp.plots.static

Trumpet Curves
--------------
.. autofunction:: trumpet_curve

Time Series
-----------
.. autofunction:: time_series

Colour-Contour Plots
--------------------
.. autofunction:: colour_contour