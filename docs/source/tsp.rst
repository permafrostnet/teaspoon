TSP classes
===========

These classes provide the core of the ``teaspoon`` package. 

.. toctree::
   :maxdepth: 2

.. automodule:: tsp.core
   :members: TSP, IndexedTSP
   :no-undoc-members:
