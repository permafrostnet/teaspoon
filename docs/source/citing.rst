Citing tsp
===========

If you use tsp in your research, please cite the following paper:

Brown, N., (2022). tsp ("Teaspoon"): A library for ground temperature data. Journal of Open Source Software, 7(77), 4704, https://doi.org/10.21105/joss.04704

BibTeX entry
^^^^^^^^^^^^

.. code-block:: tex

    @article{Brown2022,
      doi = {10.21105/joss.04704}, 
      url = {https://doi.org/10.21105/joss.04704}, 
      year = {2022}, 
      publisher = {The Open Journal},
      volume = {7},
      number = {77}, 
      pages = {4704}, 
      author = {Nicholas Brown},
      title = {tsp ("Teaspoon"): A library for ground temperature data},
      journal = {Journal of Open Source Software}
    } 