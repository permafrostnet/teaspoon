.. _readers:

Readers
========


.. automodule:: tsp.readers
   :members:
   :no-undoc-members:
   :show-inheritance:
